%% ME 405 Lab 6 Matlab Code

syms x theta x_dot theta_dot T_x 

% Initialize the constants with the proper units
r_m = .060; % [m]
l_r = .050; % [m]
r_B = .0105; % [m]
r_G = .042; % [m]
l_P = .110; % [m]
r_P = .0325; % [m]
r_C = .050; % [m]
m_B = .030; % [kg]
m_P = .400; % [kg]
I_P = 1.88e6 * 1e-9; % [kg*m^2]
b = 10*1e-3; % [mNm*s/rad]
I_B = (2/5)*m_B*r_B^2; % [kg*m^2]
g = 9.81; % [m/s^2]

M = [-(m_B*r_B^2 + m_B*r_C*r_B + I_B)/r_B, -(I_B*r_B + I_P*r_B + m_B*r_B^3 + m_B*r_B*r_C^2 + 2*m_B*r_B^2*r_C + m_P*r_B*r_G^2 + m_B*r_B*x^2)/r_B;
    -(m_B*r_B^2 + I_B)/r_B, -(m_B*r_B^3 + m_B*r_C*r_B^2 + I_B*r_B)/r_B];

f = [b*theta_dot - g*m_B*(sin(theta)*(r_B + r_C) + x*cos(theta)) + T_x*l_P/r_m + 2*m_B*theta_dot*x*x_dot - g*m_P*r_G*sin(theta);
    -m_B*r_B*x*theta_dot^2 - g*m_B*r_B*sin(theta)];

q_ddot = M\f;
% display(q_ddot);

% Creating state vector (i.e. x = [qdot; q])
x_ss = [x_dot;
        theta_dot;
        x;
        theta];
% Creating derivative of state vector  (i.e. dx/dt = [q_ddot; qdot])
xdot_ss = [q_ddot(1);
           q_ddot(2);
           x_dot;
           theta_dot];

% Creating Jacobian Jx, which is dg/dx, by using the jacobian() function
% built into MATLAB
dgdx = jacobian(xdot_ss, x_ss);


% Creating Jacobian Ju, which is dg/du, by using the jacobian() function
% built into MATLAB
dgdu = jacobian(xdot_ss, T_x);

%% Open Loop Simulation (Equilibrium)

% Creating initial conditions for positions, velocities, and input torque.
% These are specific to this Case A. For the next cases, each will have
% their specific initial conditions given from the lab handout.
IC = [0, 0, 0, 0]; % initial condition array is ordered in the following way: [x_dot, theta_dot, x, theta]
IC_u = 0; % initial input represents [T_x]

% Creating state matrix (A) and input matrix (B)
A = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC, IC_u]));
B = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC, IC_u]));

% Creating state matrix (C) and input matrix (D)
C = eye(4); % creating 4x4 identity matrix using eye(n)
D = 0;

% Creating a continuous-time state-space mode object
sys = ss(A, B, C, D);
% Creating an array representing time
t1 = 0:0.01:1.0;
% Creating an array representing input for our system. The input is an
% array of 0s
u1 = zeros(1,101);

% plots the simulated time response of the dynamic system model "sys" to
% the input history (t1, u1) with specified vector of initial state values
% "IC"
OL = lsim(sys, u1, t1, IC); % The column order of OL is [x_dot, theta_dot, x, theta]

% Creates figure with the 4 subplots representing position, angle,
% velocity, and angular velocity, all with respect to time
figure(1);
subplot(4,1,1);
plot(t1, OL(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Case A: Position vs Time')

subplot(4,1,2);
plot(t1, OL(:,4));
ylabel('theta [rad]');
xlabel('t [s]');
title('Case A: Angle vs Time');

subplot(4,1,3);
plot(t1, OL(:,1));
ylabel('x dot [m/s]');
xlabel('t [s]');
title('Case A: Velocity vs Time');

subplot(4,1,4);
plot(t1, OL(:,2));
ylabel('theta dot [rad/s]');
xlabel('t [s]');
title('Case A: Anglular Velocity vs Time');

%% Open Loop Simulation (0.05m from CoG of platform)

IC_2 = [0, 0, 0.05, 0];
A2 = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC_2, IC_u]));
B2 = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC_2, IC_u]));

sys2 = ss(A2, B2, C, D);
t = 0:0.01:0.4;
u = zeros(1,41);
OL2 = lsim(sys2, u, t, IC_2);

figure(2);
subplot(4,1,1);
plot(t, OL2(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Case B: Position vs Time');

subplot(4,1,2);
plot(t, OL2(:,4));
ylabel('theta [rad]')
xlabel('t [s]')
title('Case B: Angle vs Time');

subplot(4,1,3);
plot(t, OL2(:,1));
ylabel('x dot [m/s]')
xlabel('t [s]')
title('Case B: Velocity vs Time');

subplot(4,1,4);
plot(t, OL2(:,2));
ylabel('theta dot [rad/s]')
xlabel('t [s]')
title('Case B: Angular Velocity vs Time');


%% Open Loop Simulation (Platform inclined at 5deg)

IC_3 = [0, 0, 0, 5*pi/180];
A3 = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC_3, IC_u]));
B3 = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC_3, IC_u]));

sys3 = ss(A3, B3, C, D);
t = 0:0.01:0.4;
u = zeros(1,41);
OL3 = lsim(sys3, u, t, IC_3);

figure(3);
subplot(4,1,1);
plot(t, OL3(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Case C: Position vs Time');

subplot(4,1,2);
plot(t, OL3(:,4));
ylabel('theta [rad]')
xlabel('t [s]')
title('Case C: Angle vs Time');

subplot(4,1,3);
plot(t, OL3(:,1));
ylabel('x dot [m/s]')
xlabel('t [s]')
title('Case C: Velocity vs Time');

subplot(4,1,4);
plot(t, OL3(:,2));
ylabel('theta dot [rad/s]')
xlabel('t [s]')
title('Case C: Angular Velocity vs Time');

%% Open Loop Simulation (includes an impulse of 1 N*m^2*s)

IC_4 = [0, 0, 0, 0];
IC_u4 = 1;
A4 = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC_4, IC_u4]));
B4 = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC_4, IC_u4]));

sys4 = ss(A4, B4, C, D);
t = 0:0.01:0.4;
u = zeros(1,41);
u(1) = 1e-3;
OL4 = lsim(sys4, u, t, IC_4);

figure(4);
subplot(4,1,1);
plot(t, OL4(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Case D: Position vs Time');

subplot(4,1,2);
plot(t, OL4(:,4));
ylabel('theta [rad]')
xlabel('t [s]')
title('Case D: Angle vs Time');

subplot(4,1,3);
plot(t, OL4(:,1));
ylabel('x dot [m/s]')
xlabel('t [s]')
title('Case D: Velocity vs Time');

subplot(4,1,4);
plot(t, OL4(:,2));
ylabel('theta dot [rad/s]')
xlabel('t [s]')
title('Case D: Angular Velocity vs Time');

%% Closed Loop Simulation (Same initial conditions as Case B for Open Loop)

K = [-0.05, -0.02, -0.3, -0.2];
A5 = A2-B2*K;
B5 = [0;
      0;
      0;
      0];
t5 = 0:0.01:25;
u5 = zeros(1,2501);
sys5 = ss(A5, B5, C, D);

CL = lsim(sys5, u5, t5, IC_2);

figure(5);
subplot(4,1,1);
plot(t5, CL(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Closed Loop: Position vs Time');

subplot(4,1,2);
plot(t5, CL(:,4));
ylabel('theta [rad]')
xlabel('t [s]')
title('Closed Loop: Angle vs Time');

subplot(4,1,3);
plot(t5, CL(:,1));
ylabel('x dot [m/s]')
xlabel('t [s]')
title('Closed Loop: Velocity vs Time');

subplot(4,1,4);
plot(t5, CL(:,2));
ylabel('theta dot [rad/s]')
xlabel('t [s]')
title('Closed Loop: Angular Velocity vs Time');


