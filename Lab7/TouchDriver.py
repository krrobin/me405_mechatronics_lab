##  @file TouchDriver.py
#  
#   Documentation for TouchDriver.py file, which is a driver that includes three individual methods that scan the X, Y, and Z components, respectively. The X and Y components are in units of millimeters. 
#   The Z component will be a boolean output of either 1 or 0. If it is 0, this tells us that there is no contact with the touch panel. If it is 1, that tells us that there is contact with the touch 
#   panel. Along with those methods, this driver also contains a method that reads all three components and returns their values as a tuple. Before these methods were created, this driver also 
#   includes a constructor that allows the user to select the four arbitrary pins that represent xp, xm, yp, and ym as well as the width and length of the resistive touch panel and the coordinate 
#   representing the center of the resistive touch panel. 
#
#
#   File can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab7/TouchDriver.py
#   
#   @author Keanau Robin
#
#   @date March 02, 2021

import pyb
from pyb import Pin
from pyb import ADC
import utime

class TouchClass:
    '''
    @brief      
    @details    
    '''
    
    def __init__(self, xp, xm, yp, ym, w, l, x0, y0):
        '''
        @brief      Allows user to select four arbitrary pins representing xp, xm, yp, and ym as well as width and length of touch panel and coordinate of center of touch panel
        @details    This constructor allows the user to select the four arbitrary pins that represent xp, xm, yp, and ym as well as the width and length of the resistive touch panel and the coordinate
                    representing the center of the resistive touch panel.
        '''
        
        ## An object copy of the pin representing xp
        self.pin_xp = xp
        
        ## An object copy of the pin representing xm
        self.pin_xm = xm
        
        ## An object copy of the pin representing yp
        self.pin_yp = yp
        
        ## An object copy of the pin representing ym
        self.pin_ym = ym
        
        ## An object copy of the width of the touch panel
        self.w = w
        
        ## An object copy of the length of the touch panel
        self.l = l
        
        ## An object copy of the zero x coordinate of the touch panel
        self.x0 = x0
        
        ## An object copy of the zero y coordinate of the touch panel
        self.y0 = y0
        
        ## Initializing my initial x value as 0
        self.init_x = 0
        
        ## Initializing my initial y value as 0
        self.init_y = 0
        
    def X_Scan(self):
        '''
        @brief      Scans the X component of the resistive touch panel
        @detail     This method scans the X component of the resistive touch panel by energizing the resistor divider between the pins representing xp and xm. The values outputted in this method are in 
                    units of millimeters.
        '''
                
        PIN_xm = Pin(self.pin_xm)
        PIN_xp = Pin(self.pin_xp)
        ADC_ym = ADC(self.pin_ym)
        PIN_ym = Pin(self.pin_ym, mode=Pin.IN)
        self.PIN_yp = Pin(self.pin_yp)
        
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_xp.init(mode=Pin.OUT_PP, value=1)
        PIN_ym.init(mode=Pin.ANALOG)
        self.PIN_yp.init(mode=Pin.ANALOG)
        ADC_ym = ADC(PIN_ym)
        
        pyb.udelay(5)
        Curr_X = ADC_ym.read()
        X_avg = (self.init_x + Curr_X)/2
        self.init_x = Curr_X
        
        X_read = ((X_avg/4095) * self.l - self.x0) #* (88/78)
        

        
        return X_read
        
    def Y_Scan(self):
        '''
        @brief      Scans the Y component of the resistive touch panel
        @detail     This method scans the Y component of the resistive touch panel by energizing the resistor divider between the pins representing yp and ym. The values outputted in this method are in 
                    units of millimeters.
        '''

        
        PIN_ym = Pin(self.pin_ym)
        ADC_xm = ADC(self.pin_xm)
        PIN_xp = Pin(self.pin_xp)
        PIN_xm = Pin(self.pin_xm, mode=Pin.IN)
        
        PIN_ym.init(mode=Pin.OUT_PP, value=0)
        self.PIN_yp.init(mode=Pin.OUT_PP, value=1)
        PIN_xm.init(mode=Pin.ANALOG)
        PIN_xp.init(mode=Pin.ANALOG)
        
        ADC_xm = ADC(PIN_xm)
        
        pyb.udelay(5)
        Curr_Y = ADC_xm.read()
        Y_avg = (self.init_y + Curr_Y)/2
        self.init_y = Curr_Y
        
        Y_read = ((Y_avg/4095) * self.w - self.y0) #* (52.5/39.3)

        return Y_read
        
    def Z_Scan(self):
        '''
        @brief      Scans the Z component of the resistive touch panel
        @detail     This method scans the Z component of the resistive touch panel by measuring the voltage at the center node while both resistor dividers are energized. An output of 0 tells us that 
                    there is no contact. An output of 1 tells us that there is contact.
        '''
        
        
        
        PIN_xm = Pin(self.pin_xm)
        PIN_yp = Pin(self.pin_yp)
        ADC_ym = ADC(self.pin_ym)
        PIN_xp = Pin(self.pin_xp)
        PIN_ym = Pin(self.pin_ym, mode=Pin.IN)
        
        PIN_yp.init(mode=Pin.OUT_PP, value=1)
        PIN_xm.init(mode=Pin.OUT_PP, value=0)
        PIN_ym.init(mode=Pin.ANALOG)
        PIN_xp.init(mode=Pin.ANALOG)
        
        ADC_ym = ADC(PIN_ym)
        
        pyb.udelay(5)
        if ADC_ym.read() > 4000:
            z_comp = 0
            
        else:
            z_comp = 1
            
        
        return z_comp
    
    def All_Scans(self):
        '''
        @brief      
        @detail     
        '''

        
        All_Comps = (self.X_Scan(), self.Y_Scan(), self.Z_Scan())

        return All_Comps
        
        
if __name__ == "__main__":
    touch = TouchClass(Pin.cpu.A7, Pin.cpu.A6, Pin.cpu.A1, Pin.cpu.A0, 100, 176, 176/2, 100/2)
    while True:
        
        start_time = utime.ticks_us()
        
        touch.All_Scans()
    
        end_time = utime.ticks_us()
        total_time = utime.ticks_diff(end_time, start_time)
        print(str(touch.All_Scans()) + ' {:}'. format(total_time))
        