
import pyb
import micropython
import urandom
import utime

micropython.alloc_emergency_exception_buf(200)
PinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
PinC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)
myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)


def myCallback(timSource):
    # print('Current time: ' + timSource.counter())
    PinA5.low()
    end = myTimer.counter()
    print(end)


extint = pyb.ExtInt (PinC13,
                     pyb.ExtInt.IRQ_FALLING,
                     pyb.Pin.PULL_UP,
                     myCallback)
    

while True:

    delay_time = int(urandom.randrange(2000,3000))
    utime.sleep_ms(delay_time)
    myTimer.counter(0)
    PinA5.high()
    # start = myTimer.counter()
    
    pyb.delay(1000)
    PinA5.low()
    # start_list.append[start]
    # end_list.append[myCallback.end]
    # reaction_time[run] = myCallback.end - start
    # run += 1