##  @file lab3main.py
#   Documentation for lab3main.py file, which contains the backend code for Lab 3. This code is constantly running on the Nucleo as it waits for a user input of G. Once the user inputs G in the Anaconda 
#   console, they must then press the blue User button located on the Nucleo board. Once the blue User button is pressed, it will trigger an interrupt that will result in data being stored inside an 
#   already created memory buffer. The data will be voltage readings from the blue User button pin. This data is being collected to observe the step response of the voltage rise. Once the voltage data 
#   has reached the desired maximum value of 4090, it is then sent to the UI_Front.py file along with corresponding time values for each voltage data.
#
#
#
#   File can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab3/main.py
#   
    
#   @author Keanau Robin
#
#   @date February 02, 2021



import pyb, micropython
import array
from pyb import UART
myuart = UART(2)

# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

## Pin object corresponding to the blue User button
PinC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

tim = 0
timestamp = [0]
Volts = [0]
n = 0

## Variable representing the User button. If True, User button was pressed.
button_pressed = None

def myCallback(pinSource):
    '''
    @brief      Callback function activated when the blue User button is pressed.
    @details    This callback function is activated when the blue User button is pressed. Once the blue User button is pressed, the callback function will over override the main code within the while 
                loop. In this callback function, the button_pressed variable will change to True, which signifies the User button being pressed.
    '''

    global button_pressed
    button_pressed = True


## Initializes an external interrupt on the falling edge for PinC13 (USER button)
extint = pyb.ExtInt (PinC13,
                      pyb.ExtInt.IRQ_FALLING,
                      pyb.Pin.PULL_NONE,
                      myCallback)

## Initializes the ADC as pin A0 in the Nucleo board
adc = pyb.ADC(pyb.Pin.board.A0)

## Initializes the timer object with a frequency of 20000Hz
timer2 = pyb.Timer(2, freq=20000)

## Initializes the memory buffer array which is initially empty
buffer = array.array('H', (0 for n in range(10000)))


#Block of code within this file that represents our data collection of the voltage data
while True:
    
    # If the UART detects a user input from the frontend
    if myuart.any() != 0:
        
        # For next two lnes of code, it declares that, in order for the next while loop to run, the user input must be 71 in ascii, which represents "G"
        var = myuart.readchar()
        if var == 71:
            
            # In this while loop, there will be a check for whether the User button was pressed. THen, data collection will begin within the ADC 
            while True:
                
                if button_pressed == True:
                    # Read analog values into buffer array at a rate set by the timer2 ojbect
                    adc.read_timed(buffer, timer2)
                    
                    # Only analog values less than 4090 will be appended into our list of data
                    while buffer[n] < 4090:
                        # appending begins when analog values greater than 30 begin to appear
                        if buffer[n] > 30:
                            Volts.append(buffer[n])
                            tim += int((1/20000)*1e6)
                            ## Timestamp is the list of times corresponding to each analog value
                            timestamp.append(tim)
                        n += 1
                        
                        # if the buffer has reached its limit of 10000, the code recollects
                        if buffer[9999] < 40:
                            adc.read_timed(buffer, timer2)
                            n = 0
                    # Once an analog value of 4090 is reached, the backend will send the list of analog values and times to the frontend for plotting
                    else:
                        myuart.write('{:};{:}'.format(timestamp, Volts))
                        break
                else:
                    pass
                
            
            
    else:
        pass
timestamp.clear()
Volts.clear()