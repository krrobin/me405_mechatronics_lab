##  @file UI_Front.py
#   Documentation for UI_Front.py file, which contains the frontend code for Lab 4. This code is what the user is interfacing with in order to collect the voltage data and plot it. This code consists 
#   of two functions. One function is for sending inputs to the backend, and the other is for receiving data from the backend. To begin data collection, the first function prompts the user to type G 
#   to begin the data collection. Once G is typed, it is sent to the backend to begin the data collection and eventually sends that data to the frontend. The second function will take that data and 
#   plot it. For further information on the functions, check the Function Documentation below.
#
#
#
#   File can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab3/UI_Front.py
#   
    
#   @author Keanau Robin
#
#   @date February 02, 2021



import serial, time
import matplotlib.pyplot as plt
import numpy as np

## Variable representing the serial communication between laptop and Nucleo
ser = serial.Serial(port='COM5',baudrate=115273,timeout=1) # same as myuart = UART(2) but in Serial because we are dealing with serial communication in the laptop, not Nucleo
## Variable representing the time values in units of microseconds
timestamp = []
## Variable representing the voltage values in units of volts
voltages = []
n = 0


def sendChar():
    '''
    @brief      This function prompts the user to input a command.
    @details    This function prompts the user to type G to begin the data collection. The ascii
                value for G will be sent to the back end and initiates the data collection. If 
                the user inputs a character that is not G, the function will prompt the user to
                type G again.
    '''
    
    while True:
        # prompt user to type G to begin collecting data
        inv = input('Please type G to begin collecting data: ')
        ## value represents the unicode code of the character that is inputted by the user
        value = ord(inv)
        # if G is not typed, user will be prompted again to type G
        if value != 71:
            inv = input('Please type G to begin collecting data: ')
        # Once G is typed, it is sent to the backend using serial communication
        else:
            ser.write(str(inv).encode('ascii'))
            break


def receiveArray():
    '''
    @brief      This function retrieves the data from the backend file for Lab 3.
    @details    This function receives the data given by the backend file for Lab 3. Once 
                data has been received, it is separated into two lists: time and voltage data.
                Then, the data is plotted Voltage vs Time.
                
    '''
    # Put a delay for 6 seconds to give the backend some time to collect the data
    time.sleep(6)
    # Read the data that the backend has sent
    line_string = ser.readline().decode('ascii')
    print(line_string)
    n = 0
    # if the length of line_string is 10 or longer, that is enough to know that there is likely decent data within the line_string
    if len(line_string) > 10:
        # strip any unneccesary square brackets and have the datain 2 rows where first row will end up being time and second row will be the voltage data
        line_list = line_string.strip('[]\r\n').split('];[')
        times = line_list[0].split(',')
        volt = line_list[1].split(',')
        # while loop that appends timestamp and voltages one by one
        while n < len(volt):
            timestamp.append(int(times[n]))
            # voltage is multiplied by 3.3/4090 to convert from the analog values to volts
            voltages.append(int(volt[n])*3.3/4090)
            n += 1
        # plot the voltage data vs time
        plt.plot(timestamp, voltages)
        plt.xlabel('Time [us]')
        plt.ylabel('Voltage [V]')
        plt.title('User Button Step response')
        plt.show()
        print('Plot has been completed!')
        # save data into a CSV file 
        np.savetxt('Lab3plot.csv', list(zip(timestamp, voltages)), fmt = '%s', delimiter = ',')
    else:
        pass


if __name__ == '__main__':
    sendChar()
    receiveArray()

ser.close()

