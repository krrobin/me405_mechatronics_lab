# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 01:02:31 2021

@author: keana
"""

import keyboard
state = 0
pushed_key = None
Seven = 0
Six = 0
Five = 0
Four = 0
Three = 0
Two = 0
One = 0
Zero = 0
balance = 0
    
def on_keypress(thing):
    """ Callback which runs when the user presses a key.
    """
    global pushed_key

    pushed_key = thing.name
     
keyboard.on_press (on_keypress)  ########## Set callback

def getChange(price, payment):
    '''
    @brief          Computes correct change for a given purchase
    @details        This function takes in a numerical value represenitng the cost of an item (i.e. beverage), along with a tuple containing the denominations being used for the purchase. The function then
                    returns change in the fewest denominations possible.
    @param price    The price is an integer representing the price of the item in dollars
    @param payment  THe payment is a tuple representing the integer number of pennies, nickles, dimes, quarters, ones, fives, tens, and twenties.
    '''

    Seven = 0
    Six = 0
    Five = 0
    Four = 0
    Three = 0
    Two = 0
    One = 0
    Zero = 0
    change = (0,0,0,0,0,0,0,0)
    payment_val = int(payment[0])/100 + int(payment[1])/20 + int(payment[2])/10 + int(payment[3])/4 + int(payment[4])*1 + int(payment[5])*5 + int(payment[6])*10 + int(payment[7])*20
    change_val = round(payment_val - float(price), 2)
    change_value = round(change_val, 2)

    if change_val < 0:
        pass
    
    elif (len(payment)!=8):
        pass

    else:
        while (change_val >= 20.00):
            change_val = change_val - 20.00
            Seven += 1
        while (change_val < 20.00 and change_val >= 10.00):
            change_val = change_val - 10.00
            Six += 1
        while (change_val < 10.00 and change_val >= 5.00):
            change_val = change_val - 5.00
            Five += 1
        while (change_val < 5.00 and change_val >= 1.00):
            change_val = change_val - 1.00
            Four += 1
        while (change_val < 1.00 and change_val >= 0.25):
            change_val = change_val - 0.25
            Three += 1
        while (change_val < 0.25 and change_val >= 0.10):
            change_val = change_val - 0.10
            Two += 1
        while (change_val < 0.10 and change_val >= 0.05):
            change_val = change_val - 0.05
            One += 1
        while (change_val < 0.05 and change_val >= 0.01):
            change_val = change_val - 0.01
            Zero += 1
        change = (Zero, One, Two, Three, Four, Five, Six, Seven)

    return (payment_val, change_value, change, change_val)

def printWelcome():
    '''
    @brief          Welcomes the user to the Vendotron vending machine
    @details        This function welcomes the user to the Vendotron. It also prompts the user to select their desired beverage and tells the user how to eject their change at any time.
    '''
    print('''Hello! Please select your beverage:
          (press C) Cuke          - $1.50
          (press P) Popsi         - $1.25
          (press S) Spryte        - $4.25
          (press D) Dr. Pupper    - $3.00
          Press E to eject your change''')

my_payment = [0,0,0,0,0,0,0,0]
while True:
    
    if state == 0: # This state prints the Welcome message
        printWelcome()
        state = 1
    
    elif state == 1:
        try:
        # If a key has been pressed, check if it's a key we care about
            if pushed_key:
                if pushed_key == "0": # For Penny
                    my_payment[0] += 1
                    balance += 0.01
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '1': # For nickel
                    my_payment[1] += 1
                    balance += 0.05
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '2': # For dime
                    my_payment[2] += 1
                    balance += 0.10
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '3': # For quarter
                    my_payment[3] += 1
                    balance += 0.25
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '4': # For one-dollar
                    my_payment[4] += 1
                    balance += 1.00
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '5': # For five-dollar
                    my_payment[5] += 1
                    balance += 5.00
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '6': # For ten-dollar
                    my_payment[6] += 1
                    balance += 10.00
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '7': # For twenty-dollar
                    my_payment[7] += 1
                    balance += 20.00
                    print(f'Current balance: ${balance:.2f}')
                    
                elif pushed_key == '8':
                    print('Invalid index. Only values 0-7.')
                    
                elif pushed_key == '9':
                    print('Invalid index. Only values 0-7.')
                    
                elif pushed_key == 'C':
                    my_price = 1.50
                    print('Beverage selected: Cuke')
                    state = 2
                elif pushed_key == 'P':
                    my_price = 1.25
                    print('Beverage selected: Popsi')
                    state = 2
                elif pushed_key == 'S':
                    my_price = 4.25
                    print('Beverage selected: Spryte')
                    state = 2
                elif pushed_key == 'D':
                    my_price = 3.00
                    print('Beverage selected: Dr. Pupper')
                    state = 2
                elif pushed_key == 'E':
                    print ("Change ejected")
                    func = getChange(0, my_payment)
                    state = 4
                pushed_key = None
        except KeyboardInterrupt:
            break
        
    elif state == 2:
        func = getChange(my_price, my_payment)
        
        if func[1] < 0: 
            print('Funds are insufficient.')
            state = 3
            
        else:
            print('Beverage has been vended! Enjoy!')
            state = 4
            
    elif state == 3: # This state is for insufficient funds
        print(f'Current balance: ${func[0]:.2f}')
        print(f'Price of chosen beverage: ${my_price:.2f}')
        print('Please insert more money and reselect your beverage.')
        state = 1
        
    elif state == 4: # This state is when the user ejects
        print('Your change:')
        print(str(func[2][0]) + ' pennies')
        print(str(func[2][1]) + ' nickels')
        print(str(func[2][2]) + ' dimes')
        print(str(func[2][3]) + ' quarters')
        print(str(func[2][4]) + ' one-dollar bills')
        print(str(func[2][5]) + ' five-dollar bills')
        print(str(func[2][6]) + ' ten-dollar bills')
        print(str(func[2][7]) + ' twenty-dollar bills')
        state = 5
        
    elif state == 5:
        print('''Thank you for choosing Vendotron! Goodbye!
              
              
              
              
              
              ''')
        my_price = 0
        my_payment = [0,0,0,0,0,0,0,0]
        # change_val = 0
        func = [(0 , 0, (0,0,0,0,0,0,0,0), 0)]
        # payment_val = 0
        Seven = 0
        Six = 0
        Five = 0
        Four = 0
        Three = 0
        Two = 0
        One = 0
        Zero = 0
        balance = 0
        state = 0