import pyb
# import array
from pyb import UART
myuart = UART(2)

while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent an ASCII ' + str(val) + ' to the Nucleo')