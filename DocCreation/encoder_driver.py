#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file encoder_driver.py

This file serves as a class file for the encoder driver class in
Python. It will implement functions and commands using the encoder.

This script has code for the encoder class that utilizes the encoder's timer. 


@brief       Code to serve as as class file for the encoder driver class in Python.
@details     This class file includes methods for getting the current position,
             setting position, get the difference between positions,
             and updating position. 
          
             Link to Source Code: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab8/encoder_driver.py
                 
@author      Ashley Humpal and Keanau Robin
@copyright   License Info
@date        March 7, 2021
"""


import pyb

## New class called encoder
class encoder:
    '''
    @brief      A finite state machine to control the encoder.
    @details    This class implements methods for set and updating the encoder readings.
    '''


      
    def __init__(self):
   
        '''
        @brief          Creates encoder object.
        
        '''
        

## The encoder object for the final position is created
        self.position= 0
        
## The encoder object for the previus position is created
        self.previousposition=0
        
## The encoder object for the period length is created
        self.period=0xFFFF
        
## The encoder object for the timer is created      
        self.tim= pyb.Timer(4) #Creating variable for the encoder timer
## The encoder object for the prescaler is created.
        self.tim.init(prescaler=0, period=self.period) # initializes timer parameters
## The encoder object for the pins are created.
        self.tim.channel(1, pin=pyb.Pin.cpu.B6, mode=pyb.Timer.ENC_AB) #sets up channel 1
## The encoder object for the encoder mode is created.

## The encoder object for B7 is created.
        self.tim.channel(2, pin=pyb.Pin.cpu.B7, mode=pyb.Timer.ENC_AB) #sets up channel 2

        
    def update(self): 
        '''
        @brief      Updates the position of the encoder by adding good delta values
        '''
## The encoder object for the current position is created
        self.currentposition= self.tim.counter() #current position
        self.delta=self.currentposition-self.previousposition
        self.previousposition=self.currentposition
## The encoder object for the delta value between runs is created.
        self.deltamag = abs(self.delta)
## The encoder object for the absolute value of delta is created

       
        if self.deltamag> .5*(self.period): 
            if self.delta<0:
               self.updateddelta = self.delta +self.period
                              
            elif self.delta>0:
                self.updateddelta = (self.delta - self.period) # Ticks/Ticks 
                #self.updateddelta = self.delta - self.period
                
                
        else:
            self.updateddelta = self.delta

        self.position= self.position+ self.updateddelta
        
    def getposition(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        # return self.tim.counter()
        return self.position
    
    def setposition(self,datum):
        '''
        @brief      Updates the variable defining the next state to run
        '''        
        return self.tim.counter(datum)
    
        #self.tim.callback(None)
    def getdelta(self):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        
        return self.updateddelta
        
    
# if __name__ == '__main__':

    
     
# ## motor 1 object created 
     
#     encoder.update()
#     encoder.getposition()
#     print(encoder.position)
