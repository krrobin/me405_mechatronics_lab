 ## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#
#  @section sec_intro Introduction
#  This is Keanau Robin's ME 405 Portfolio. It contains code for all lecture and lab assignments for ME 405.
#
#
#  Included modules are:
#  * Lab0x01 (\ref sec_lab1)
#  * Lab0x02 (\ref sec_lab2)
#  * Lab0x03 (\ref sec_lab3)
#  * Lab0x04 (\ref sec_lab4)
#  * Lab0x05 (\ref sec_lab5)
#  * Lab0x06 (\ref sec_lab6)
#  * Lab0x07 (\ref sec_lab7)
#  * Lab0x08 (\ref sec_lab8)
#  * Lab0x09 (\ref sec_lab9)
#
#
#
#  @section sec_lab1 Vending Machine (Lab 1)
#  This section contains the file for Lab 1 Vending Machine. The file contains code that implements a FSM to mimic a vending machine. The user inputs a payment and their desired beverage. If the funds are
#  sufficient, the vending machine will vend the beverage and give the user their change. The program is meant to run indefinitely until the user types ctrl + c into the console.
#  
#  Click VendingMachine.py for further information regarding the code.
#  
#  Source code located here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab1/
#
#  @image html Lab1_StateTransitionDiagram.PNG
#
#
#  @section sec_lab2 Reaction Time (Lab 2)
#  This section contains the file for Lab 2 Reaction Time. This program interfaces with a Nucleo L476RG board to measure the reaction time of the user. The program starts with a random time between 2 to 3
#  seconds. After that time period has passed, the green LED (LD2) will light up and the timer will begin counting. The LED will remain on for a maximum of 1 second. Within that 1 second, the user is meant
#  to press the blue User button on the Nucleo which will turn off the LED and store the reaction time of the user. If the user does not press the button within that 1 second, there will not be a reaction 
#  time recorded for that run, and an error statement will be sent to the user. The user can do as many runs as they please. When they are content with the amount of runs they have completed, they must 
#  press ctrl + C to interrupt the program and receive their average reaction time from all of the runs.
#
#  Click ReactionTime.py for further information regarding the program.
#
#  Source code located here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab2/
# 
#
#  @section sec_lab3 Voltage Step Response (Lab 3)
#  This section contains the files for Lab 3 Voltage Step Response. This code implements serial communication between the laptop and the Nucleo L476RG bord to collect the voltage step response created when 
#  the blue USER button on the Nucleo is pressed. The program starts with the UI_Front.py file, which prompts the user to type G to begin the data collection. Once the user inputs G, the UI_Front.py sends 
#  the user input (G) to the lab3main.py file, which will activate the data collection. The user must then press the blue User button represented by pin C13. The voltage rise caused by that button press is
#  then collected as analog values. An ADC (analog to digital conversion) object will be used to obtain this data. Once the data is collected, it is sent to the UI_Front.py where it will be plotted and 
#  saved in a CSV file.
#
#  It is important to note that I collaborated with Kyle Chuang and Ashley Humpal for this lab.
#
#  Source code located here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab3/
#
#  Source for the CSV File: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab3/Lab3plot.csv
#
#  Documentation located here: UI_Front.py, lab3main.py
#
#  The following image is the task diagram for Lab 3 that shows the tasks and communciation between the front and backend.
#  
#  @image html Lab3_TaskDiagram.PNG
#
#
#  @section sec_lab4 Hot or Not? (Lab 4)
#  It is very important to note that Josephine Isaacson was my partner for this lab.
#
#  This section contains the files for Lab 4 Hot or Not. This code implements I2C communication between the Nucleo and mcp9808 temperature sensor to collect two sets of data: internal MCU temperature of 
#  the Nucleo and the ambient temperature of the room using the mcp9808 temperature sensor. The mcp9808.py file is the mcp9808 driver that contains the check(), celsius(), and fahrenheit() methods. For 
#  more information on each method, please click mcp9808.py. The lab4main.py file contains the code that implements the mcp9808 driver methods, I2C communication, and ADCAll to collect the desired data 
#  for this lab. Once data collection is complete, all data is saved in a CSV file labeled me405lab4.csv.
#
#  Source code can be located in this shared repository: https://bitbucket.org/krrobin/me405_lab4_isaacson_robin/
#
#  Documentation located here: mcp9808.py, lab4main.py
#
#  The following image is a plot of the internal MCU temperature vs Time and Ambient temperature vs Time. There is a total of 480 data points for each data set. The data collection went on for 8 hours 
#  with data being collected every minute. These temperature measurements were made from 8pm to 4am in my insulated room.
#
#  @image html Lab4Plot.PNG
#
#
#  @section sec_lab5 Feeling Tipsy? (Lab 5)
#  For this lab, hand calculations were made, which can be found in this link: https://krrobin.bitbucket.io/me405/Lab5HandCalculations.html
#  
#  
#  @section sec_lab6 Simulation or Reality? (Lab 6)
#  For this lab, plots were created using MATLAB. These plots can be found in this link: https://krrobin.bitbucket.io/me405/Lab6Plots.html
#
#  The MATLAB code used to generate our plots can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab6/Lab6MATLAB.m
#
#
#  @section sec_lab7 Feeling Touchy (Lab 7)
#  It is important to note that I worked with Ashley Humpal and Kyle Chuang to troubleshoot the code for this lab.
#  
#  This section contains the driver file for Lab 7, which will also act as our driver file for the Winter 2021 Term Project. The driver includes three individual methods that scan the X, Y, and Z 
#  components of the 8" resistive touch panel. For more documentation on the driver file itself, please click TouchDriver.py. 
#
#  Within the resistive touch panel, there are two resistor dividers. Depending on the desired component reading, one or both of the resistor dividers are energized. When scanning the X component, 
#  the resistor divider between the xp and xm must be energized. When scanning the Y component, the resistor divider between the yp and ym must be energized. When scanning the Z component, the 
#  voltage at the center node must be measured while both resistor dividers are energized.
#  
#  Source code located here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab7/TouchDriver.py
#
#  Documentation located here: TouchDriver.py
#
#  The following image is the schematic of the connection between the Touch Panel, FPC breakout board, and the Top Panel. The FPC breakout board was soldered onto the double-ended Pin-header, which was then 
#  soldered onto the Top Panel.
#  
#  @image html Lab7FPCboard.PNG
#
#  As we noted during lab, the pin setup will differ between each individual student. The following list shows which FPC pins correspond to which CPU pins.
#  
#  * X-    ---> PA6
#  * Y-    ---> PA0
#  * X+   ---> PA7
#  * Y+   ---> PA1
#
#  The next images will show my hardware setup. Note that the tape was temporarily used to maintain balance for my touch panel. The first two images show the setup for the FPC cable connecting the 
#  Touch Panel to the FPC Breakout Board.
#
#  @image html Lab7FPCsetup1.jpg
#
#  @image html Lab7FPCsetup2.jpg
#
#  @image html Lab7FPCsetup3.jpg
#  
#  
#  The next image included the hardware setup of the Touch Panel, along with its coordinate axes and maximum x and y coordinates. It is important to note that the listed dimensions for the Touch Panel 
#  were 176mm in the x direction and 107mm on the y direction. However, due to variations in the hardware, I measured my x direction to be a total of 154mm (from adding the max values in the image, 
#  which are 78.2mm and 75.8mm) and my y direction to be a total of 78.4mm (from adding the max values in the image, which are 39.7mm and 38.7mm). The tape on the Touch Panel indicate where the 
#  active area ends. To be more specific, once the ball touches the tape, the readings are no longer valid values. I used a red line to indicate the edges of all four pieces of tape. 
#  
#  @image html Lab7TouchPanel.jpg
#
#
#  A requirement of our drivers is for it to measure the positional coordinates of the ball under the required time of 1500 microseconds. The following data verifies that the driver does so. My driver's 
#  average time was at about 1450 microseconds, which is within the requirement. Within this data, the 4 digit value to the right represent the time for each run in units of microseconds. The values 
#  in the parentheses represent the (X, Y, Z) components.
#
#
#
#
#  @code{.py}
#   (10.27204, 17.00855, 1) 1438
#   (10.25056, 16.97192, 1) 1439
#   (10.25056, 16.99634, 1) 1449
#   (10.29353, 16.97192, 1) 1439
#   (10.20757, 16.98413, 1) 1437
#   (10.25056, 16.88645, 1) 1452
#   (10.27204, 16.97192, 1) 1437
#   (10.29353, 16.99634, 1) 1439
#   (10.29353, 17.00855, 1) 1450
#   (10.25056, 16.99634, 1) 1449
#   (10.10012, 17.00855, 1) 1436
#   (10.27204, 16.97192, 1) 1448
#   (10.22906, 16.97192, 1) 1446
#   (10.29353, 16.95971, 1) 1451
#   (10.1646, 17.04517, 1) 1437
#   (10.25056, 16.98413, 1) 1438
#   (10.29353, 16.97192, 1) 1439
#   (10.25056, 16.98413, 1) 1451
#   (10.22906, 16.99634, 1) 1502
#   (10.25056, 16.95971, 1) 1496
#   (10.29353, 16.99634, 1) 1446
#   (10.29353, 17.00855, 1) 1449
#   (10.29353, 16.98413, 1) 1453
#   (10.25056, 16.98413, 1) 1437
#   (10.25056, 16.98413, 1) 1439
#   (10.22906, 16.97192, 1) 1439
#   (10.27204, 16.91087, 1) 1450
#   (10.27204, 16.99634, 1) 1438
#   (10.20757, 16.98413, 1) 1452
#   (10.40098, 16.83761, 1) 1438
#   (10.22906, 16.99634, 1) 1451
#   @endcode
#
#
#  @section sec_lab8 Term Project Part 1 (Lab 8)
#  It is important to note that my partner for this lab was Ashley Humpal. My partner's documentation is located here: https://ahumpal.bitbucket.io.
#
#  For this lab, we created two separate drivers: motor_driver.py and encoder_driver.py. In the motor driver, it allows us to control the motors connected to our Nucleo board. In the encoder driver, it 
#  allows us to check the position of the encoder angle.
#
#  For further documentation of the driver files, please click the following: motor_driver.py and encoder_driver.py
#
#  Source code for the motor driver and encoder driver can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab8/
#
#
#  @section sec_lab9 Term Project Part 2 (Lab 9)
#  It is important to note that my partner for this lab was Ashley Humpal. My partner's documentation is located here: https://ahumpal.bitbucket.io.
#
#  
#
#
#  @author Keanau Robin
#
#  @date March 15, 2021
#  
#  @page Lab5HandCalculations Lab 5 Hand Calcs
#  @section sec_lab5details Lab 5 Hand Calculations
#  Within this page, there are hand calculations required to analyze/describe the motion of the pivoting platform of our hardware as it attempts to balance a ball. It is very important to note that 
#  Josephine Isaacson and Keanau Robin partnered up to complete these hand calculations.
#
#  The following image is the CAD model of the pivoting platform we are analyzing.
#
#  @image html Lab5CAD.PNG
#
#  For this lab, we were given the following parameters and figures to follow as we do our hand calculations.
#  
#  @image html Lab5Param1.PNG
#  @image html Lab5Param2.PNG
#  @image html Lab5Param3.PNG
#
#  The following 5 pages are the hand calculations for Lab 5.
#  
#  @image html Lab5Page1.jpg
#  @image html Lab5Page2.jpg
#  @image html Lab5Page3.jpg
#  @image html Lab5Page4.jpg
#  @image html Lab5Page5.jpg
#  
#  @author Keanau Robin
#
#  @date February 16, 2021
#
#
#  @page Lab6Plots Lab 6 Plots
#  @section sec_lab6details Lab 6 Plots
#  Within this page, there are plots that represent position, angle, velocity, and angular velocity of the ball and balancing platform when given multiple scenarios. It is very important to note that Josephine 
#  Isaacson and Keanau Robin partnered up to complete these plots.
#
#  The first simulations we did involved an open-loop model. For the very first case, Case A, the ball is initially at rest on a level platform directly above the center of gravity of the platform and 
#  there is no torque input from the motor. What should happen in the plots is that all four subplots should be a flat line at 0 because there is no movement in the model. The subplots below confirm this 
#  reasoning.
#
#  @image html Lab6Plot1.PNG
#
#
#  For the second case, Case B, the ball is initially at rest on a level platform offset horizontally from the center of gravity of the platform by 0.05m and there is no torque input from the motor. In 
#  the subplots, we should see the platform angle increase because of the offset position of the ball, which also results in an increase in angular velocity. The position of the ball will also move 
#  further away from the center of gravity of the platform, which results in an increase in velocity. The subplots below confirm this reasoning.
#
#  @image html Lab6Plot2.PNG
#
#
#  For the third case, Case C, the ball is initially at rest on a platform inclined at 5deg directly above the center of gravity of the platform and there is no torque input from the motor. In the 
#  subplots, we should see the platform angle starts at the nonzero value of 5deg and quickly increase in angle due to the ball rolling down. This also results in an increase in angular velocity as
#  the simulation goes on. The ball position will start at the center of gravity and quickly offset from that center of gravity due to the incline of the platform. This will result in an increase in
#  velocity. The subplots below confrim this reasoning.
#
#  @image html Lab6Plot3.PNG
#
#
#  For the fourth case, Case D, the ball is initially at rest on a level platform directly above the center of gravity of the platform and there is an impluse of 1 mNm*s applied by the motor. In the
#  subplots, we should see the platform angle and ball position decrease, but very slowly due to their resistance to change in position (aka inertial properties of each object). Due to the initial 
#  push by the motor, there will be an initial peak for the velocity of the ball. As can be seen from the subplots below, this reasoning can be confirmed.
#
#  @image html Lab6Plot4.PNG
#
#
#  Now, we will look into the closed loop simulation. This simulation has the same initial conditions as Case B above. The closed loop simulation implements a regulator using full state feedback. We 
#  were given a pre-made controller for this simulation. In the subplots, we should see all of them approahc zero after about 20-25 seconds due to that closed loop feedback. From our subplots, we can
#  confirm that this reasoning is valid.
#
#  @image html Lab6Plot5.PNG
#
#
#  @author Keanau Robin
#
#  @date February 23, 2021