##  @file ReactionTime.py
#   Documentation for ReactionTime.py, which contains code that measures the user's average reaction time. The program waits a random time between 2 and 3 seconds before the green LED lights up. The 
#   green LED stays on for only 1 second. The user must press the blue User button within that 1 second of light or else a reaction time will not be stored for that specific run and an error message
#   will be displayed. The user can attempt as many runs as they'd like. Once they are content with the amount of runs they have done, the user can then press Ctrl + C to stop the program and receive
#   their average reaction time.
#
#
#   File can be found here: https://bitbucket.org/krrobin/me405_mechatronics_lab/src/master/Lab2/
#   
#   @author Keanau Robin
#
#   @date January 26, 2021


# Import any required modules
import pyb
import micropython
import utime
import urandom

# Create emergency buffer to store errors that are thrown
# inside ISR
micropython.alloc_emergency_exception_buf(200)

reac_time = []
timestamp = 0

myTimer = pyb.Timer(2, prescaler=79, period=0x7FFFFFFF)

def myCallback(timSource):
    '''
    @brief          Callback function used when the user clicks the blue User button.
    @details        This callback function is used when the user clicks the blue User button. This will break the while loop within the program and turn the LED off.
    '''
    PinA5.low()
    global timestamp 
    timestamp = myTimer.counter()

PinA5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)

PinC13 = pyb.Pin(pyb.Pin.board.PC13, mode=pyb.Pin.IN)

extint = pyb.ExtInt (PinC13,
                     pyb.ExtInt.IRQ_FALLING,
                     pyb.Pin.PULL_UP,
                     myCallback)


while True:
    try:
        delay_time = int(urandom.randrange(2000, 3000))
        utime.sleep_ms(delay_time)
        myTimer.counter(0)
        timestamp = 0
        PinA5.high()        
        pyb.delay(1000)
        PinA5.low()
        if timestamp < 1000000 and timestamp > 0:
            reac_time.append(timestamp)
            print('Current reaction time: ' + str(reac_time))
        else:
            print('Reaction time was not fast enough, so it was not recorded. Please press within one second of the LED lighting up.')

    except KeyboardInterrupt:
        avg_time = sum(reac_time) / len(reac_time)
        print('All Reaction Times [microseconds]: ' + str(reac_time))
        print('Average Reaction Time: ' + str(avg_time) + ' microseconds')
        break
    