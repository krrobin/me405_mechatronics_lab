
"""
@file Lab9main.py

@brief       Code to serve as a controller to balance a ball on the resistive touch screen.
@details     This main script incorporates classes from the TouchDriver.py, motor_driver.py, and encoder_driver.py.
             It uses these files to get the x, y, theta y, theta x, theta dot x, theta dot y, xdot, and ydot 
             readings of our system. Those are used to calculate the current torque neccessary to balance the ball and plate,
             and then that torque is converted into a duty cycle that the motors run at. 
             Additonally, if the ball ever leaves the platform, our motors automatically disable as a safety precaution. 
             
             Link to Source Code: 
                 
@author      Ashley Humpal and Keanau Robin
@copyright   License Info
@date        March 15, 2021

"""
# needed imports
import utime 
import pyb
import math

from screendriver import touchscreen
from motor_driver import MotorDriver
from encoder_driver import encoder

## Object representing the resistive touch panel
touch = touchscreen(pyb.Pin.cpu.A7,pyb.Pin.cpu.A6,pyb.Pin.cpu.A1,pyb.Pin.cpu.A0, .176, .100, .176/2, .100/2)
## Object representing motor 1 which corresponds to movement in the x direction
moe1 = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B4, 1, pyb.Pin.cpu.B5, 2, 3) # Motor for x axis (next to battery port)
## Object representing motor 2 which corresponds to movement in the y direction
moe2 = MotorDriver(pyb.Pin.cpu.A15, pyb.Pin.cpu.B0, 3, pyb.Pin.cpu.B1, 4, 3) # Motor for y axis 
## Encoder object corresponding to motor 1 
enc1 = encoder(pyb.Pin.cpu.B6, pyb.Pin.cpu.B7, 4)
## Encoder object corresponding to motor 2
enc2 = encoder(pyb.Pin.cpu.C6, pyb.Pin.cpu.C7, 8)


moe1.enable() #turn on power

enc1.update() #get current encoder value

enc2.update() #get current encoder value

## Variable representing start time 
start_time = utime.ticks_ms() #start tracking time

## Variable representing intial x coordinate
x = touch.scanX()
## Variable representing intial y coordinate
y = touch.scanY()

## Theta_y in units of radians
theta_y = enc1.getposition() * 2 * math.pi / 4000
## Theta_x in units of radians
theta_x = enc2.getposition() * 2 * math.pi / 4000


## Controller gain for x or y dot
K1 = -4.5915 * 250
## Controller gain for theta-dot x or y 
K2 = -0.3546 * 100
## Controller gain for x or y 
K3 = -11.5758 * 100
## Controller gain for theta x or y 
K4 = -5.0706

## Motor Resistance 
R = 2.21 # units of ohms
## Powersupply voltage
Vdc = 12 # units of volts
## Torque/Back EMF constant
Kt = 13.8 # Units of mNm/A

## Conversion to go from Torque to Duty
duty_conv = R * 100 / (Vdc * Kt)


enc1.setposition(0) # zero encoder 1
enc2.setposition(0) # zero encoder 2
while True:
    try:
        # Create current time variable and time span variable
## Variable representing current time in ms 
        curr_time = utime.ticks_ms()
## Variable representing delta t
        timespan = utime.ticks_diff(curr_time,start_time) # take different of times 
        
## Variable representing a new x coordinate value
        x_new = touch.scanX() # current run value for x 
## Variable representing a new x dot  value
        x_dot = (x_new - x)/timespan # current run value for x dot
        x = x_new #setting old x to this runs x val to iterate
        
        # Create theta_y_new and theta_dot_y
        enc2.update() #get current enc value
## Variable representing current theta y in rad
        theta_y_new = enc2.getposition() * 2 * math.pi / 4000
## Variable representing current theta dot y in rad/s
        theta_dot_y = (theta_y_new - theta_y) / timespan
        theta_y = theta_y_new  #setting old theta y to this runs theta y val to iterate
        
        # Create Torque_y and duty cycle for motor 2
## Variable representing Torque needed for motor 2
        Torque_y = -K1*x_dot - K2*theta_dot_y - K3*x - K4*theta_y
        #print('1st, 2nd, 3rd, 4th: ' + str(K1*x_dot) + ';' + str(K2*theta_dot_y) + ';' + str(K3*x) + ';' + str(K4*theta_y))
## Duty needed for Motor 1 in y direction
        Duty_y = int(duty_conv * Torque_y) #convert torque to duty 
        moe2.set_duty(Duty_y) #set duty 
        #print('y Duty: ' + str(Duty_y))
        
        
        
        
        # Create y_new variable and y dot

## Variable representing a new y coordinate value
        y_new = touch.scanY() # current run value for y
## Variable representing a new y dot  value        
        y_dot = (y_new - y)/timespan
        y = y_new  #setting old x to this runs x val to iterate
        
        # Create theta_x_new and theta_dot_x
        enc1.update() #get current enc value
## Variable representing current theta x in rad
        theta_x_new = enc1.getposition() * 2 * math.pi / 4000
## Variable representing current theta dot x in rad/s
        theta_dot_x = (theta_x_new - theta_x) / timespan
        theta_x = theta_x_new #setting old theta x to this runs theta x val to iterate
        
        # Create Torque_x and duty cycle for motor 2
## Variable representing Torque needed for motor 2
        Torque_x = -K1*y_dot - K2*theta_dot_x - K3*y - K4*theta_x
## Duty needed for Motor 2 in x direction
        Duty_x = int(duty_conv * Torque_x) #convert torque to duty 
        moe1.set_duty(Duty_x)
        #print('x Duty: ' + str(Duty_x))
        
        
        start_time = curr_time #reseting  time values to iterate 
        
        if touch.scanZ() == 0: # if no ball on screen
            moe1.set_duty(0) #set motor duty 1 to 0
            moe2.set_duty(0)  #set motor duty 2 to 0
            moe1.disable() #disable motor
            print('Ball not detected in active area. Disabling motors')
            break
        else:
            pass
        
    except KeyboardInterrupt: #emergency stop in case all else fails 
        moe1.set_duty(0) # set duties to 0 
        moe2.set_duty(0)
        
        moe1.disable() #disable
        break