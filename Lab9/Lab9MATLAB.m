%% ME 405 Lab 9 Matlab Code

syms x theta x_dot theta_dot T_x 

% Initialize the constants with the proper units
r_m = .060; % [m]
l_r = .050; % [m]
r_B = .0105; % [m]
r_G = .042; % [m]
l_P = .110; % [m]
r_P = .0325; % [m]
r_C = .050; % [m]
m_B = .030; % [kg]
m_P = .400; % [kg]
I_P = 1.88e6 * 1e-9; % [kg*m^2]
b = 10*1e-3; % [mNm*s/rad]
I_B = (2/5)*m_B*r_B^2; % [kg*m^2]
g = 9.81; % [m/s^2]

M = [-(m_B*r_B^2 + m_B*r_C*r_B + I_B)/r_B, -(I_B*r_B + I_P*r_B + m_B*r_B^3 + m_B*r_B*r_C^2 + 2*m_B*r_B^2*r_C + m_P*r_B*r_G^2 + m_B*r_B*x^2)/r_B;
    -(m_B*r_B^2 + I_B)/r_B, -(m_B*r_B^3 + m_B*r_C*r_B^2 + I_B*r_B)/r_B];

f = [b*theta_dot - g*m_B*(sin(theta)*(r_B + r_C) + x*cos(theta)) + T_x*l_P/r_m + 2*m_B*theta_dot*x*x_dot - g*m_P*r_G*sin(theta);
    -m_B*r_B*x*theta_dot^2 - g*m_B*r_B*sin(theta)];

q_ddot = M\f;


% Creating state vector (i.e. x = [qdot; q])
x_ss = [x_dot;
        theta_dot;
        x;
        theta];
% Creating derivative of state vector  (i.e. dx/dt = [q_ddot; qdot])
xdot_ss = [q_ddot(1);
           q_ddot(2);
           x_dot;
           theta_dot];

% Creating Jacobian Jx, which is dg/dx, by using the jacobian() function
% built into MATLAB
dgdx = jacobian(xdot_ss, x_ss);


% Creating Jacobian Ju, which is dg/du, by using the jacobian() function
% built into MATLAB
dgdu = jacobian(xdot_ss, T_x);


% Creating initial conditions for positions, velocities, and input torque.
% These initial conditions are specific to the case where the ball is at
% the origin and the whole system is in equilibrium.
IC = [0, 0, 0, 0]; % initial condition array is ordered in the following way: [x_dot, theta_dot, x, theta]
IC_u = 0; % initial input represents [T_x]

% Creating state matrix (A) and input matrix (B)
A = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC, IC_u]));
B = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC, IC_u]));

%% Finding K values
% Must run Case 1 to get A and B values before running this section of the
% code
syms K1 K2 K3 K4 s

% Creating our gain matrix K
K = [K1 K2 K3 K4];

% Creating our closed loop matrix A
A_cl = A - B*K;

% Creating identity with equivalent matrix dimensions as the closed loop
% mtrix A
I = eye(length(A_cl));

% Creating the characteristic polynomial for the system under closed loop
% feedback
P_cl = det(s*I - A_cl);

% Getting the coefficients of each power of s, which will be used for
% polynomial matching
P_cl_coeff = coeffs(P_cl, s);


%% Desired System Model

% natural frequency [rad/s]
w_n = 31.42;
% damping ratio [unitless]
damp = 0.9;

% a, b, and c are the coefficients of the second order differential
% equation used for finding the pole locations for the desired second order
% system.
a = 1;
b = 2 * w_n * damp;
c = w_n^2;
P = [a b c];

% Found the poles of the second order system above
poles = roots(P);

% Made a variable for the four desired pole locations where the last two
% poles are about 10x larger than the real component of the two poles
% previously found. 
poles_des = [ poles
             -280 + 0i
             -285 + 0i];

% Here is where we used the four poles to evaluate the characteristic
% polynomial for the desired fourth-order system
P_des = (s^2 + b*s + c) * (s + 280) * (s + 285);

% Here we extracted the coefficients from the characteristic polynomial
P_des_coeff = coeffs(P_des, s);
P_des_coeff_a = vpa(P_des_coeff(4));
P_des_coeff_b = vpa(P_des_coeff(3));
P_des_coeff_c = vpa(P_des_coeff(2));
P_des_coeff_d = vpa(P_des_coeff(1));
%% Characteristic Polynomial Coefficient Matching
format short
syms K1 K2 K3 K4

% Here we equated the desired system's coefficients with those from the
% closed loop system
eqn1 = (2286923775254233*K1)/70368744177664 - (6185651633222423*K2)/8796093022208 + 8637419007845129/2251799813685248 == P_des_coeff_a;
eqn2 = (2286923775254233*K3)/70368744177664 - (166577182280392125289*K1)/7229252916776563748060511292358656 - (6185651633222423*K4)/8796093022208 - 67117678274168649/1125899906842624 == P_des_coeff_b;
eqn3 = - (48800718073225264058876151517565*K1)/9903520314283042199192993792 - (1324711698855699*K2)/9903520314283042199192993792 - (166577182280392125289*K3)/7229252916776563748060511292358656 - 343450413828797967991/115668046668425019968968180677738496 == P_des_coeff_c;
eqn4 = - (48800718073225264058876151517565*K3)/9903520314283042199192993792 - (1324711698855699*K4)/9903520314283042199192993792 - 31335384717709517578611818784839/39614081257132168796771975168 == P_des_coeff_d;

% Here we solve for the K gains within the equations created above
eqn = solve([eqn1, eqn2, eqn3, eqn4], [K1, K2, K3, K4]);

% These are our K gains used in the main code for Lab 9
Sol_a = vpa(eqn.K1);
Sol_b = vpa(eqn.K2);
Sol_c = vpa(eqn.K3);
Sol_d = vpa(eqn.K4);

% Combining the K gains found from coefficient matching
Sol = [Sol_a Sol_b Sol_c Sol_d]

%% Final Confirmation using place()

% Using place() to check the results of our K gains
PoleCheck = place(A, B, poles_des)

%% Closed Loop Simulation to check K value performance using plots

% Creating initial conditions for positions, velocities, and input torque.
% These are specific to the case where ball is displaced along the x axis at about 50mm.
IC_2 = [0, 0, 0.05, 0];

% Creating state matrix (A) and input matrix (B)
A2 = double(subs(dgdx, [x_dot, theta_dot, x, theta, T_x], [IC_2, IC_u]));
B2 = double(subs(dgdu, [x_dot, theta_dot, x, theta, T_x], [IC_2, IC_u]));

% THis is the K Gain matrix with our gains created from the code above
K = [-1029.0877, -48.4370, -15987.5898, -899.2598];

A5 = A2-B2*K;
B5 = [0;
      0;
      0;
      0];

% Creating state matrix (C) and input matrix (D)
C = eye(4); % creating 4x4 identity matrix using eye(n)
D = 0;

% Creating an array representing time
t5 = 0:0.01:2;

% Creating an array representing input for our system. The input is an
% array of 0s
u5 = zeros(1,201);

% Creating a continuous-time state-space mode object
sys5 = ss(A5, B5, C, D);

% plots the simulated time response of the dynamic system model "sys5" to
% the input history (t5, u5) with specified vector of initial state values
% "IC_2"
CL = lsim(sys5, u5, t5, IC_2); % The column order of CL is [x_dot, theta_dot, x, theta]

% Creates figure with the 4 subplots representing position, angle,
% velocity, and angular velocity, all with respect to time
figure(1);
subplot(4,1,1);
plot(t5, CL(:,3));
ylabel('x [m]')
xlabel('t [s]')
title('Closed Loop: Position vs Time');

subplot(4,1,2);
plot(t5, CL(:,4));
ylabel('theta [rad]')
xlabel('t [s]')
title('Closed Loop: Angle vs Time');

subplot(4,1,3);
plot(t5, CL(:,1));
ylabel('x dot [m/s]')
xlabel('t [s]')
title('Closed Loop: Velocity vs Time');

subplot(4,1,4);
plot(t5, CL(:,2));
ylabel('theta dot [rad/s]')
xlabel('t [s]')
title('Closed Loop: Angular Velocity vs Time');

